import { Router } from "express";
import Equipe from '../model/equipe.js'
const equipeRouter=Router();
import {getAllEquipe,getByIdEquipe,addEquipe,deleteEquipe,specUpdateEquipe} from '../controller/equipeController.js'
//les routes
//get all teams
equipeRouter.get('/',getAllEquipe);
//get a team by id
equipeRouter.get('/:id',getByIdEquipe);
//add new team
equipeRouter.post('/',addEquipe);
//delete a team
equipeRouter.delete('/:id',deleteEquipe)
equipeRouter.put('/:id',specUpdateEquipe);









export default equipeRouter;