import { Router } from "express";
import Joueur from '../model/joueur.js'
const joueurRouter=Router();
import joueurController from "../controller/joueurController.js";
//les routes
//get all teams
joueurRouter.get('/',joueurController.getAllJoueur);
//get a player by id
joueurRouter.get('/:id',joueurController.getByJoueur);
//add new player
joueurRouter.post('/',joueurController.addJoueur);
//delete a player
joueurRouter.delete('/:id',joueurController.deleteJoueur)
joueurRouter.put('/:id',joueurController.specUpdateJoueur);

joueurRouter.get('/joueursEquipe/:id',joueurController.getJoueursParEquipe);








export default joueurRouter;